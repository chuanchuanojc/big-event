$(function () {

  //点击登录页面去注册页面
  $('#link-login').on('click', function () {
    //登录页面隐藏
    $('#login').hide()
    //注册页面显示
    $('#reg').show()
  })

  //点击注册页面去登录页面
  $('#link-reg').on('click', function () {
    //注册页面隐藏
    $('#reg').hide()
    //登录页面显示
    $('#login').show()
  })

  //为layui.form 校验组件 添加  添加校验规则
  layui.form.verify({
    //添加密码校验规则
    pwd: [
      /^[\S]{6,12}$/
      , '密码必须6到12位，且不能出现空格'
    ],
    //添加判断两次密码规则
    repwd: function (value) {
      if ($('#reg [name="password"]').val() !== value) {
        return '两次密码不一样哦~~'
      }
    }

  })

  //注册表单 提交事件
  $('#reg_form').on('submit', function (e) {
    //阻止表单默认事件
    e.preventDefault()
    //发送异步请求
    $.ajax({
      type: 'POST',
      url: '/api/reguser',
      data: {
        username: $('#reg [name="username"]').val(),
        password: $('#reg [name="password"]').val()
      },
      success: (res) => {
        console.log(res);
        //判断是否成功
        if (res.status !== 0) return layui.layer.msg(res.message)
        //注册成功后
        layui.layer.msg('注册成功了~ 马上进去登录页面啦~~ ', {
          icom: -1,
          time: 1000
        }, () => {
          //触发 去登陆 对象的点击事件
          $('#link-reg').trigger('click')

          console.log(this);
          //前面两个用箭头函数  没有this this 指绑定事件对象
          this.reset()
        })
      }
    })
  })

  //登录表单  提交事件
  $('#login_form').on('submit', function (e) {
    //阻止表单默认事件
    e.preventDefault()
    //发送异步请求
    $.ajax({

      type: 'POST',
      url: '/api/login',
      data: {
        username: $('#login [name="username"]').val(),
        password: $('#login [name="password"]').val()
      },
      success: (res) => {
        console.log(res);
        //判断是否成功
        if (res.status !== 0) return layui.layer.msg(res.message)
        //注册成功后
        layui.layer.msg('登录成功了~ 马上进入主页面啦~~ ', {
          icom: -1,
          time: 1000
        }, () => {
          //登录成功后 把token 保存到本地
          localStorage.setItem('token', res.token)
          //登录成功后 跳转到主页面
          window.location.replace('/index.html')

          console.log(this);
          //前面两个用箭头函数  没有this this 指绑定事件对象
          this.reset()
        })
      }
    })
  })

})