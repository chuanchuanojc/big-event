
$(function () {
  let dd = document.querySelectorAll('#indexUserDl dd')

  //a.获取用户信息并渲染页面
  getUserMessage()

  //b. 绑定退出按钮事件
  $('#logout').on('click', loginout)

  //4.切换个人资料 切换高亮
  // $('#indexUserDl dd').on('click', function () {
  //   this.classList.add('layui-this')
  // })

  $('#titleUserData dd').on('click', function () {
    //清除其他样式
    $('#homePage').removeClass('layui-this')
    $('#indexUserDl dd').removeClass('layui-this')


    // $('#personalCenter').on('click', function (e) {
    //   this.classList.add('layui-nav-itemed')
    // })
    //添加类名
    $('#personalCenter').addClass('layui-nav-itemed')

    //模拟点击
    // document.querySelectorAll('#indexUserDl dd')[this.dataset.id].click()
    //添加类名
    dd[this.dataset.id].classList.add('layui-this')

  })
})


//1.获取用户信息
function getUserMessage() {
  $.ajax({
    method: 'GET',
    url: '/my/userinfo',
    success: function (res) {
      console.log(res);
      //判断获取信息 是否成功
      //获取信息失败
      if (res.status !== 0) return
      //获取信息成功
      renderAvatar(res.data)
    }
  })
}


//2.渲染头像
function renderAvatar(data) {
  //渲染名称
  const userName = data.nickname || data.username
  //设置到页面
  $('#welcome').html('欢迎您  ' + userName)
  console.log(userName);
  //渲染头像
  if (data.user_pic) {
    //显示自己的头像
    $('.layui-nav-img').attr('src', data.user_pic).show()
    //隐藏字体头像
    $('.userportrait').hide()

  } else {
    //隐藏自己的头像
    $('.layui-nav-img').hide()
    //显示字体头像
    $('.userportrait').html(userName[0]).css('display', 'inline-block')

  }

}

//3.退出事件绑定
function loginout() {

  layui.layer.confirm('亲，您确认退出吗', function (index) {
    //删除本地token
    localStorage.removeItem('token')
    //跳转到login页面
    location.replace('../../login.html')

    layer.close(index);
  });

}