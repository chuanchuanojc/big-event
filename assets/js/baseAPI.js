//根路径 
const root = 'http://api-breakingnews-web.itheima.net'

$.ajaxPrefilter(function (opt) {
  //1.拼接根路径
  opt.url = root + opt.url

  //2.判断请求的端口是否是/my/
  if (opt.url.includes('/my/')) {
    opt.headers = {
      Authorization: localStorage.getItem('token')
    }
  }

  //3.身份验证失败回调函数
  opt.complete = function (res) {
    // console.log(res); //status: 1, message: "身份认证失败！"
    //通过返回的res 中的responseJSON 来判断
    let text = res.responseJSON
    // console.log(text);
    if (text.status === 1 && text.message === '身份认证失败！') {
      //如果身份验证失败 给用户提示并进行下一步操作
      layui.layer.alert(text.message, { closeBtn: 0 }, function (index) {
        //清除本地token
        localStorage.removeItem('token')
        //跳转到login页面
        window.parent.location.replace('/login.html')

        layer.close(index)
      })
    }
  }

})