$(function () {

  //1.定义加载文章类别方法
  getDataArticleCate()
  function getDataArticleCate() {
    //发送请求
    $.ajax({
      method: 'GET',
      url: '/my/article/cates',
      success: function (res) {
        //请求失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //请求成功 调用引擎米板
        let dataStr = template('tpl-cates', res)
        //渲染页面
        $('select[name="cate_id"]').html(dataStr)
        layui.form.render()
      }
    })
  }
  //2.初始化富文本编辑器
  initEditor()
  //3.初始化裁剪区
  // 3.1 初始化图片裁剪器
  var $image = $('#image')

  // 3.2 裁剪选项
  var options = {
    aspectRatio: 400 / 280,
    preview: '.img-preview'
  }

  // 3.3 初始化裁剪区域
  $image.cropper(options)

  //4. 模拟点击事件
  $('#chooseImgBtn').on('click', function () {
    $('#coverfile').trigger('click')
  })
  //5.监听选择图片 改变事件
  $('#coverfile').on('change', function (e) {
    //获取选择文件列表
    let files = e.target.files
    //如果用户没有选着文件 直接return
    if (files.length === 0) return
    // 通过文件获取新的路径
    let newUrl = URL.createObjectURL(files[0])
    //设置新的路径 并销毁以前的路径
    $image
      .cropper('destroy')      // 销毁旧的裁剪区域
      .attr('src', newUrl)  // 重新设置图片路径
      .cropper(options)        // 重新初始化裁剪区域
  })
  //6.发布文章提交事件
  //初始化文章状态
  let artStatu = ''
  //点击不同的按钮 赋不同的值
  $('#draftBtn').on('click', function () {
    artStatu = '草稿'
  })
  $('#releaseBtn').on('click', function () {
    artStatu = '已发布'
  })


  $('#artPubForm').on('submit', function (e) {
    //阻止默认事件
    e.preventDefault()
    //获取  获取数据的表单 
    let form = document.querySelector('#artPubForm')
    //创建From Data 对象  初始化
    let fd = new FormData(form)
    //添加文章状态属性
    fd.append('state', artStatu)
    // 存入裁剪的图片
    $image
      .cropper('getCroppedCanvas', { // 创建一个 Canvas 画布
        width: 400,
        height: 280
      })
      .toBlob(function (blob) {
        fd.append('cover_img', blob)
        // 将 Canvas 画布上的内容，转化为文件对象
        // 得到文件对象后，进行后续的操作
        //准备好数据 发送请求
        $.ajax({
          method: 'POST',
          url: '/my/article/add',
          data: fd,
          contentType: false,
          processData: false,

          success: function (res) {
            layui.layer.msg(res.message)
            //请求失败
            if (res.status !== 0) return
            //请求成功 掉转页面
            location.href = '/article/art_list.html'
            //切换类名
            //删除其他类名
            window.top.$('#indexArtDl dd').removeClass('layui-this')
            //添加类名
            window.top.$('#artList').addClass('layui-this')

          }
        })
      })


  })

})