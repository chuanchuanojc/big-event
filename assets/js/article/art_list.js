
$(function () {
  //1.参数对象-----------------------------------------------
  const p = {
    pagenum: 1,//默认页码  默认第一页
    pagesize: 2,//页容量   默认每页2条
    cate_id: '', //筛选分类 id
    state: ''//文章分布状态
  }

  //2.获取数据-----------------------------------------------
  getDataArticleList()
  function getDataArticleList() {
    //发送请求
    $.ajax({
      method: 'GET',
      url: '/my/article/list',
      data: p,
      success: function (res) {
        //请求失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //请求成功 调用引擎米板
        let dataStr = template('tpl-list', res)
        //渲染页面
        $('tbody').html(dataStr)
        //渲染分页
        renderPage(res.total)
      }
    })
  }

  // 3.为模板引擎 添加 日期格式化过滤器 ------------------------
  template.defaults.imports.dataFormat = function (date) {
    const dt = new Date(date);
    let y = dt.getFullYear();
    let m = padZero(dt.getMonth() + 1);
    let d = padZero(dt.getDate());

    let hh = padZero(dt.getHours());
    let mm = padZero(dt.getMinutes());
    let ss = padZero(dt.getSeconds());
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`;
  }
  function padZero(num) {
    return num >= 10 ? num : '0' + num
  }

  //4.求情文章分类 ------------------------------------------
  getDataArticleCate()
  function getDataArticleCate() {
    //发送请求
    $.ajax({
      method: 'GET',
      url: '/my/article/cates',
      success: function (res) {
        //请求失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //请求成功 调用引擎米板
        let dataStr = template('tpl-cates', res)
        //渲染页面
        $('select[name="cate_id"]').html(dataStr)
        layui.form.render()
      }
    })
  }

  //5.为搜索表单绑定事件--------------------------------------
  $('#formSearch').on('submit', function (e) {
    //阻止默认
    e.preventDefault()
    //更新参数对象
    p.cate_id = $('select[name="cate_id"]').val()
    p.state = $('select[name="state"]').val()
    //重新请求服务器发布文章
    getDataArticleList()

  })

  //6.渲染页码函数-------------------------------------------
  // function renderPage(total) {
  //   //调用layui 的laypage 内置对象
  //   layui.laypage.render({
  //     elem: 'pageBox',      //渲染的地方
  //     count: total,         //渲染数据总条数
  //     limit: p.pagesize,    //每页数据条数
  //     curr: p.pagenum,       //默认页码
  //     layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
  //     limits: [2, 3, 4, 5],
  //     //点击分页回调
  //     jump: function (obj, first) {
  //       //获取当前点击的页码
  //       p.pagenum = obj.curr
  //       //获取页容量
  //       p.pagesize = obj.limit
  //       //判断是自动触发 还是手动触发
  //       if (!first) {
  //         //重新渲染页面
  //         getDataArticleList()
  //       }
  //     }
  //   })
  // }
  //6(1) 渲染 分页 页码
  function renderPage(total) {
    //调用layui 方法渲染页面
    layui.laypage.render({
      elem: 'pageBox',
      count: total,
      limit: p.pagesize,
      curr: p.pagenum,
      layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
      limits: [2, 3, 4, 5],
      jump: function (obj, first) {
        //获取页码  设置给参数对象
        p.pagenum = obj.curr
        p.pagesize = obj.limit
        //判断 是那种方式 触发的事件
        if (!first) {
          getDataArticleList()
        }

      }
    })

  }



  //7.绑定删除点击事件  事件代理------------------------------
  $('tbody').on('click', ('#delBtn'), function () {
    //获取本次点击页面 删除按钮的个数
    let num = $('.btn-delete').length
    console.log(num);
    //获取id
    const id = this.dataset.id
    //给用户提示是否删除 本数据
    layui.layer.confirm('您确认删除本篇文章吗?', { icon: 3, title: '提示' }, function (index) {
      //发送异步请求 删除数据
      $.ajax({
        method: 'GET',
        url: '/my/article/delete/' + id,
        success: function (res) {
          //删除失败
          if (res.status !== 0) return layui.layer.msg(res.message)
          //日说删除的时候 删除按钮只剩一个 就让p对象中的 pagenum 减1    如果已经是一 就不减
          p.pagenum = num === 1 ? p.pagenum - 1 : p.pagenum
          //删除成功后重新渲染页面
          getDataArticleList()

        }
      })
      layer.close(index);
    });

  })

})

