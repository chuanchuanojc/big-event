
$(function () {
  getDataListArticle()

  //1.获取数据渲染页面
  function getDataListArticle() {
    //请求服务器
    $.ajax({
      method: 'GET',
      url: '/my/article/cates',
      success: function (res) {
        //获取失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //获取成功 调用模板引擎
        let dataListStr = template('tpl-list', res.data)
        //渲染页面
        $('tbody').html(dataListStr)

      }
    })

  }

  //2.添加分类按钮 点击事件
  $('#addaArticleCate').on('click', function () {
    //显示弹出层
    layui.layer.open({
      //弹出框类型
      type: 1,
      area: ['500px', '249px'],
      title: '添加文章分类',
      content: $('#add-cate').html()
    })
  })

  //3.添加提交事件  后面生成的元素  使用事件委托
  $('body').on('submit', '#addArticleCate', function (e) {
    //阻止表单默认提交
    e.preventDefault()
    //获取表单数据
    const dataStr = $('#addArticleCate').serialize()
    //发送异步请求
    $.ajax({
      method: 'POST',
      url: '/my/article/addcates',
      data: dataStr,
      success: function (res) {
        //请求失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //请求成功 强制刷新页面
        location.reload()


      }
    })
  })

  //4.添加点击事件  修改类别   
  $('tbody').on('click', '#editBtn', function () {
    //点击  弹出 修改层
    layui.modifyPopupId = layui.layer.open({
      //弹出框类型
      type: 1,
      area: ['500px', '249px'],
      title: '修改文章分类',
      content: $('#modify').html()
    })
    //获取id
    const idStr = this.parentNode.dataset.id

    //异步发送请求
    $.ajax({
      method: 'GET',
      url: '/my/article/cates/' + idStr,
      success: function (res) {
        //请求失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //请求成功
        layui.form.val('modify-form', res.data)
      }
    })
  })

  //5.添加提交事件  提交修改
  $('body').on('submit', '#modify-form', function (e) {
    //阻止默认提交
    e.preventDefault()
    //发送异步请求
    const dataStr = $(this).serialize()
    $.ajax({
      method: 'POST',
      url: '/my/article/updatecate',
      data: dataStr,
      success: function (res) {
        //请求失败
        if (res.status !== 0) return layui.layer.msg(res.message)
        //请求成功 关闭弹出层 重新渲染页面
        layui / layer.close(layui.modifyPopupId)
        getDataListArticle()

      }
    })

  })

  //5.删除 文章
  $('tbody').on('click', '#delBtn', function () {
    //弹出确认框
    layui.layer.confirm('您确认删除这天数据吗?', (index) => {
      //确认删除
      //获取id 
      $.ajax({
        method: 'GET',
        url: '/my/article/deletecate/' + this.parentNode.dataset.id,
        success: function (res) {
          if (res.status !== 0) return layui.layer.msg(res.message)
          //重新渲染页面
          getDataListArticle()

        }

      })


      layer.close(index);
    })
  })


})

