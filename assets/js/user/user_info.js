
$(function () {
  //1.渲染页面表单内容
  getUserMessageFrom()

  //2.添加自定义规则
  layui.form.verify({
    nickname: [
      /^[\S]{2,6}$/
      , '昵称必须是2-6个字符哦~~'
    ]
  })

  //3.添加修改用户事件
  $('.layui-form').on('submit', doSubmit)
  //4.添加重置点击事件
  $('#restBtn').on('click', function () {
    getUserMessageFrom()
  })

})


function getUserMessageFrom() {
  $.ajax({
    method: 'GET',
    url: '/my/userinfo',
    success: function (res) {
      //判断获取信息 是否成功
      //获取信息失败
      if (res.status !== 0) return

      //获取信息成功
      //调用layui   form.val 方法
      layui.form.val('userForm', res.data)

    }
  })
}


function doSubmit(e) {
  //阻止默认提交
  e.preventDefault()
  //异步请求
  const dataStr = $(this).serialize()
  console.log(dataStr);
  $.ajax({
    method: 'POST',
    data: dataStr,
    url: '/my/userinfo',
    success: function (res) {
      console.log(res);
      //如果修改不成功
      if (res.status !== 0) return layui.layer.msg(res.message)

      //修改成功
      layui.layer.msg(res.message)

      window.top.getUserMessage()
    }
  })

}