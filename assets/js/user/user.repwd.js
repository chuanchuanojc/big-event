
$(function () {
  //1.给保单添加校验规则
  layui.form.verify({
    //密码校验
    repwd: [
      /^[\S]{6,12}$/
      , '密码必须是6-12个字符哦~~'
    ],
    //确认密码校验
    confirmPwd: function (value) {
      if (value !== $('[name="newPwd"]').val().trim()) {
        return '两次密码不一样哦~~'
      }
    }
  })
  //2. 发送异步请求 修改密码
  $('.layui-form').on('submit', doSubmitRePwd)

})

//请求服务器修改密码
function doSubmitRePwd(e) {
  //阻止默认提交
  e.preventDefault()
  //发送异步请求
  const dataStr = $(this).serialize()
  $.ajax({
    method: 'POST',
    data: dataStr,
    url: '/my/updatepwd',
    success: function (res) {
      //判断请求是否成功
      if (res.status !== 0) return layui.layer.msg(res.message, { icon: -1, time: 1000 })
      //请求成功
      layui.layer.msg(res.message, { icon: -1, time: 1000 }, function () {
        //清除本地token值
        localStorage.removeItem('token')
        //跳转到登录页面
        window.top.location.replace('/login.html')

      })

    }
  })

}