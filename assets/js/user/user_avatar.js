

$(function () {
  // 1.1 获取裁剪区域的 DOM 元素
  var $image = $('#image')
  // 1.2 配置选项
  const options = {
    // 纵横比
    aspectRatio: 1,
    // 指定预览区域
    preview: '.img-preview'
  }

  // 1.3 创建裁剪区域
  $image.cropper(options)

  //1.4 模拟点击
  $('#btnChooseImg').on('click', function () {
    $('#btnFile').trigger('click')
  })

  //1.5给文件选择框添加change事件
  $('#btnFile').on('change', function (e) {
    //获取选取文件信息
    const files = e.target.files
    //创建虚拟路劲
    const newImgUrl = URL.createObjectURL(files[0])
    //设置新路径
    $image
      .cropper('destroy')      // 销毁旧的裁剪区域
      .attr('src', newImgUrl)  // 重新设置图片路径
      .cropper(options)        // 重新初始化裁剪区域
  })

  //1.6 给上传头像 添加点击事件
  $('#btnOK').on('click', function () {
    //获取图片地址
    var dataURL = $image
      .cropper('getCroppedCanvas', { // 创建一个 Canvas 画布
        width: 100,
        height: 100
      })
      .toDataURL('image/png') // 将 Canvas 画布上的内容，转化为 base64 格式的字符串

    //异步发送请求
    $.ajax({
      method: 'POST',
      url: '/my/update/avatar',
      data: { avatar: dataURL },
      success: function (res) {
        //给用户提示
        layui.layer.msg(res.message)
        //如果失败 就return
        if (res.status !== 0) return
        // 如果成功重新渲染头像
        window.top.getUserMessage()

      }
    })
  })
})